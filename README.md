# Description 


# How To Use
1. Best way to get working with the code is to open it in Eclipse
2. You will need to have Java 1.8 installed and Ant
3. Load it up in eclipse and go to the Ant window (Window -> Show View -> Ant) and click on "Run the Default Target"  
	> If you do not want to use Eclipse, just run "ant build.xml"
4. 


# Workflows

## Updating mohawk.converter.generated.MohawkTARBAC.g4 (Using Eclipse)
1. Update the file mohawk.converter.generated.MohawkTARBAC.g4 and save (you can ignore if it auto compiles)
2. In the ant window in eclipse run "mohawk_converter -> parser" (where mohawk_converter is the build.xml name)
3. Go to the project "mohawk_converter" (I called it 'Mohawk-T Conversion Tool') and "refresh it":   
	> Right click on the project and click "Refresh" or click on the project and click 'F5' (the single key)   
	> This step is very important
4. Check for any errors that appear in the Java code located in mohawk.converter.generated.*
5. Open up the JUnit window and run the tests associated with the mohawk conversion tool   
	> If you do not see mohawk.converter.testing.RegressionTests then find the Java file and right click it and go to "Run As" and click on "JUnit Test"
6. Repeat until all tests have past

## Updating mohawk.converter.generated.MohawkTARBAC.g4 (Using Commandline)
NOTE: the scripts are currently only tested on windows, so some tweaking may have to be made (like replacing 'cmd' with 'bash')

1. Update the file mohawk.converter.generated.MohawkTARBAC.g4 and save 
2. Run "ant build regression", check that the output is all correct (it builds the source code and then runs the JUnit tests)
3. Repeat until done

# Troubleshooting

## RegressionTests.java cannot see other Project Files (in Eclipse)
This is a weird bug with eclipse only, check the ANT build file first to make sure it can build the JAR file properly.

To solve this problem:

1. Right click the project -> Build Path -> Configure Build Path
2. Go to the Libraries Tab
3. Click "Add Library" and then click on JUnit
4. Make sure to install JUnit 4 or higher   
	> I doubt a lower version would give you trouble, but JUnit 4 is what is being used to build the JAR file in the ANT script

# Contact
Author: Jonathan Shahen <jonathan.shahen@gmail.com>

# Notes
* In markdown if you want to start a new line you must end the line with two or more spaces [BitBucket Issue - The text editor is losing line breaks](https://bitbucket.org/site/master/issue/7396/the-text-editor-is-losing-line-breaks)
* Eclipse cannot do nested lists, but Bitbucket can: [BitBucket Tutorial - Markdown Demo](https://bitbucket.org/tutorials/markdowndemo/overview#markdown-header-links)