<!--
	@author Jonathan Shahen
-->
<project name="mohawkT.converter.reverse" default="dist" basedir=".">
	<description>Mohawk-T Reverse Converter Tool.</description>

	<!-- set global properties for this build -->
	<property name="src" location="src" />
	<property name="lib" location="lib" />
	<property name="bin" location="bin" />
	<property name="data" location="data" />
	<property name="logs" location="logs" />
	<property name="build" location="${bin}/build" />
	<property name="dist" location="${bin}/dist" />
	<property name="antlr" location="${lib}/antlr-4.4-complete.jar" />
	<property name="junit" location="${lib}/junit-4.12.jar" />
	<property name="qtest" location="${data}/" />

	<property name="parser-package" value="mohawk.converter.reverse.generated" />

	<!-- Customize this line to point to the 'Mohawk-T Globals' Project's source folder -->
	<property name="src.globals" location="../Mohawk-T Globals/src" />
	<property name="src.globals.parsers" location="../Mohawk-T Globals/parsers" />

	<path id="test.classpath">
		<pathelement location="${build}" />
		<pathelement location="${junit}" />
		<fileset dir="${lib}">
			<include name="**/*.jar" />
		</fileset>
	</path>

	<target name="init">
		<!-- create the bin directory -->
		<mkdir dir="${bin}" />
		<!-- create the build directory -->
		<mkdir dir="${build}" />
		<!-- create the distribution directory -->
		<mkdir dir="${dist}" />
		<!-- create the logs directory -->
		<mkdir dir="${logs}" />
	</target>

	<target name="compile" depends="init" description="compile the source ">
		<!-- compile the java code from ${src} into ${build} -->
		<javac srcdir="${src}" destdir="${build}" debug="on" deprecation="true">
			<src path="${src}" />
			<src path="${src.globals}" />
			<src path="${src.globals.parsers}" />
			<classpath>
				<fileset dir="${lib}">
					<include name="**/*.jar" />
				</fileset>
			</classpath>
		</javac>
	</target>

	<target name="javadoc">
		<javadoc access="private" author="true" classpath="lib/commons-lang3-3.3.2.jar;lib/antlr-4.4-complete.jar;lib/stringtemplate.jar;lib/hamcrest-core-1.3.jar;lib/junit-4.12.jar;lib/commons-cli-1.2.jar" destdir="javadoc" nodeprecated="false" nodeprecatedlist="false" noindex="false" nonavbar="false" notree="false" packagenames="mohawk.converter.generated,mohawk.converter.to_uzun,mohawk.converter.logging,mohawk.converter.testing,mohawk.converter.to_mohawk,mohawk.converter,mohawk.converter.pieces,mohawk.converter.to_ranise" source="1.8" sourcepath="src" splitindex="true" use="true" version="true" />
	</target>

	<target name="dist" depends="compile" description="generate the distribution">
		<!-- put everything in ${build} into the jar file -->
		<jar destfile="${dist}/${ant.project.name}.jar" basedir="${build}">
			<manifest>
				<attribute name="Built-By" value="${user.name}" />
				<attribute name="Main-Class" value="mohawk.converter.Converter" />
				<attribute name="Class-Path" value="." />
			</manifest>
			<fileset dir="${build}" />
			<zipfileset src="${antlr}" />
			<zipfileset src="${lib}/commons-cli-1.2.jar" />
			<zipfileset src="${lib}/commons-lang3-3.3.2.jar" />
			<zipfileset src="${lib}/hamcrest-core-1.3.jar" />
			<zipfileset src="${junit}" />
			<zipfileset src="${lib}/stringtemplate.jar" />
		</jar>
	</target>

	<target name="qtest" depends="dist" description="generate a JAR file that will be placed close to the data for quick testing">
		<!-- put everything in ${build} into the jar file -->
		<copy file="${dist}/${ant.project.name}.jar" tofile="${qtest}" />
	</target>

	<target name="regression" depends="dist" description="Performs a regression test">
		<!-- @author Jonathan Shahen -->
		<junit haltonfailure="true" printsummary="true">
			<test name="mohawk.converter.reverse.testing.RegressionTests" />
			<formatter type="plain" usefile="false" />
			<classpath refid="test.classpath" />
		</junit>

		<!-- OLD Windows ONLY running JUnit through the commandline
		<exec executable="cmd" dir="." failonerror="true" osfamily="windows">
			<arg value="/c" />
			<arg value="java -cp &quot;${dist}/${ant.project.name}.jar&quot; org.junit.runner.JUnitCore mohawk.converter.testing.RegressionTests" />
		</exec>
		-->
	</target>

	<target name="clean" description="clean up">
		<!-- delete the ${build} and ${dist} directory trees -->
		<delete dir="${bin}" />
		<delete>
			<fileset dir="." defaultexcludes="no">
				<include name="**/*.*~" />
			</fileset>
		</delete>
	</target>
</project>
