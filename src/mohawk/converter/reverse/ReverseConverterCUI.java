package mohawk.converter.reverse;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Logger;

import org.apache.commons.cli.Options;

public class ReverseConverterCUI {
    public static final Logger logger = Logger.getLogger("mohawk");
    public static String previousCommandFilename = "ReverseConverterCUIPreviousCommand.txt";

    public static void main(String[] args) {
        ReverseConverterInstance inst = new ReverseConverterInstance();
        ArrayList<String> argv = new ArrayList<String>();
        String cmd = "";
        Scanner user_input = new Scanner(System.in);

        Options options = new Options();
        inst.setupOptions(options);
        inst.printHelp(options, 120);

        printCommonCommands();

        System.out.print("Enter Commandline Argument ('!exit' to run): ");
        String quotedStr = "";
        StringBuilder fullCommand = new StringBuilder();
        while (true) {
            cmd = user_input.next();
            fullCommand.append(cmd + " ");

            if (quotedStr.isEmpty() && cmd.startsWith("\"")) {
                System.out.println("Starting: " + cmd);
                quotedStr = cmd.substring(1);
                continue;
            }
            if (!quotedStr.isEmpty() && cmd.endsWith("\"")) {
                System.out.println("Ending: " + cmd);
                argv.add(quotedStr + " " + cmd.substring(0, cmd.length() - 1));
                quotedStr = "";
                continue;
            }

            if (!quotedStr.isEmpty()) {
                quotedStr = quotedStr + " " + cmd;
                continue;
            }

            if (cmd.equals("!exit")) {
                break;
            }
            argv.add(cmd);
        }
        user_input.close();

        System.out.println("Commands: " + argv);

        FileWriter fw;
        try {
            fw = new FileWriter(previousCommandFilename, false);
            fw.write(fullCommand.toString());
            fw.close();
        } catch (IOException e) {
            System.out.println("[ERROR] Unable to write out previous command to: " + previousCommandFilename);
        }

        inst.run(argv.toArray(new String[1]));
    }

    public static void printCommonCommands() {

        System.out.println("\n\n--- Common Commands ---");
        System.out.println(OptionString.FROM_MOHAWK.c() + OptionString.SPECFILE.c("data/Mohawk/positive/test01.mohawk")
                + OptionString.LOGLEVEL.c("debug") + "!exit");
        System.out.println(OptionString.FROM_MOHAWK.c() + OptionString.SPECFILE.c("data/Mohawk/positive/test10.mohawk")
                + OptionString.LOGLEVEL.c("debug") + "!exit");
        System.out.println(OptionString.FROM_MOHAWK.c() + OptionString.SPECFILE.c("data/Mohawk/mixed/test01.mohawk")
                + OptionString.LOGLEVEL.c("debug") + "!exit");
        System.out.println(OptionString.FROM_MOHAWK.c() + OptionString.SPECFILE.c("data/Mohawk/mixed/test09.mohawk")
                + OptionString.LOGLEVEL.c("debug") + "!exit");
        System.out.println(
                OptionString.FROM_ASASPTIME_NSA.c() + OptionString.SPECFILE.c("data/TestsuiteC/AGTHos01.asaptime.nsa")
                        + OptionString.LOGLEVEL.c("debug") + "!exit");
        System.out.println(
                OptionString.FROM_ASASPTIME_NSA.c() + OptionString.SPECFILE.c("data/TestsuiteC/AGTHos08.asaptime.nsa")
                        + OptionString.LOGLEVEL.c("debug") + "!exit");
        System.out.println(
                OptionString.FROM_ASASPTIME_NSA.c() + OptionString.SPECFILE.c("data/TestsuiteC/AGTUniv01.asaptime.nsa")
                        + OptionString.LOGLEVEL.c("debug") + "!exit");
        System.out.println(
                OptionString.FROM_ASASPTIME_NSA.c() + OptionString.SPECFILE.c("data/TestsuiteC/AGTUniv08.asaptime.nsa")
                        + OptionString.LOGLEVEL.c("debug") + "!exit");
        System.out
                .println(OptionString.FROM_ASASPTIME_SA.c() + OptionString.SPECFILE.c("data/TestsuiteB/c01.asaptime.sa")
                        + OptionString.LOGLEVEL.c("debug") + "!exit");
        System.out
                .println(OptionString.FROM_ASASPTIME_SA.c() + OptionString.SPECFILE.c("data/TestsuiteB/c10.asaptime.sa")
                        + OptionString.LOGLEVEL.c("debug") + "!exit");

        System.out.println("");

        System.out.println(OptionString.FROM_MOHAWK.c() + OptionString.SPECFILE.c("data/Mohawk/positive/")
                + OptionString.LOGLEVEL.c("debug") + OptionString.BULK.c() + "!exit");
        System.out.println(OptionString.FROM_MOHAWK.c() + OptionString.SPECFILE.c("data/Mohawk/mixednocr/")
                + OptionString.LOGLEVEL.c("debug") + OptionString.BULK.c() + "!exit");
        System.out.println(OptionString.FROM_MOHAWK.c() + OptionString.SPECFILE.c("data/Mohawk/mixed/")
                + OptionString.LOGLEVEL.c("debug") + OptionString.BULK.c() + "!exit");
        System.out.println(OptionString.FROM_ASASPTIME_NSA.c() + OptionString.SPECFILE.c("data/TestsuiteC/")
                + OptionString.LOGLEVEL.c("debug") + OptionString.BULK.c() + "!exit");
        System.out.println(OptionString.FROM_ASASPTIME_SA.c() + OptionString.SPECFILE.c("data/TestsuiteB/")
                + OptionString.LOGLEVEL.c("debug") + OptionString.BULK.c() + "!exit");

        System.out.println("");
        try {
            BufferedReader bfr = new BufferedReader(new FileReader(previousCommandFilename));
            String previousCmd = bfr.readLine();
            bfr.close();
            System.out.println("Previous Command: " + previousCmd);
        } catch (IOException e) {
            System.out.println("[ERROR] Unable to load previous command!");
        }
    }
}
