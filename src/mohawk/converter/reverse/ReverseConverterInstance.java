package mohawk.converter.reverse;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.*;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.cli.*;
import org.apache.commons.lang3.StringUtils;

import mohawk.converter.reverse.testing.BooleanErrorListener;
import mohawk.global.FileExtensions;
import mohawk.global.formatter.MohawkCSVFileFormatter;
import mohawk.global.formatter.MohawkConsoleFormatter;
import mohawk.global.helper.FileHelper;
import mohawk.global.parser.asasptime.nsa.ASASPTimeNSALexer;
import mohawk.global.parser.asasptime.nsa.ASASPTimeNSAParser;
import mohawk.global.parser.asasptime.sa.ASASPTimeSALexer;
import mohawk.global.parser.asasptime.sa.ASASPTimeSAParser;
import mohawk.global.parser.mohawk.v2.Mohawkv2Lexer;
import mohawk.global.parser.mohawk.v2.Mohawkv2Parser;
import mohawk.global.pieces.MohawkT;
import mohawk.global.timing.MohawkTiming;

public class ReverseConverterInstance {
    private static final String VERSION = "v1.1.1";
    private static final String AUTHORS = "Jonathan Shahen <jmshahen@uwaterloo.ca>";
    // Logger Fields
    public static final Logger logger = Logger.getLogger("mohawk");
    private String Logger_filepath = "mohawk-converter-reverse.csv";
    private String Logger_folderpath = "logs";
    private ConsoleHandler consoleHandler = new ConsoleHandler();
    private Level LoggerLevel;
    private FileHandler fileHandler;
    private Boolean WriteCSVFileHeader = true;
    private String timingFile = "logs/latest_Mohawk-T_Reverse_Converter_Timing_Results.csv";

    // Helpers
    public FileHelper fileHelper = new FileHelper();
    public MohawkTiming timing = new MohawkTiming();
    public File inputFolder = new File(".").getAbsoluteFile();
    public FileExtensions fileExt = new FileExtensions();
    /** Flag to printout the stats of the Mohawk-T file after conversion. Does not put in log, only to console!
     * 
     * Turn off when using in production */
    public boolean displayStats = true;
    /** Flag to write out the stats of the Mohawk-T file after conversion. */
    public boolean writeStats = true;
    public String statsFile = "logs/latest_Mohawk-T_Reverse_Converter_Stats.csv";
    public boolean writeMohawkTOutput = true;

    public int run(String[] args) {
        try {
            Options options = new Options();
            setupOptions(options);

            CommandLineParser cmdParser = new BasicParser();
            CommandLine cmd = cmdParser.parse(options, args);

            setupLoggerOptions(cmd, options);

            if (setupReturnImmediatelyOptions(cmd, options)) { return 0; }

            /* Timing */timing.startTimer("totalTime");

            setupUserPreferenceOptions(cmd, options);

            setupSpecOptions(cmd, options);

            ///////////////////////////////
            /* *** Done With Options *** */
            ///////////////////////////////

            // TIMING
            // Check if the timing file exists to know if the header should be written
            File _timingFile = new File(timingFile);
            Boolean writeTimingHeader = false;
            if (!_timingFile.exists()) {
                writeTimingHeader = true;
            }

            FileWriter timingWriter = new FileWriter(_timingFile, true);
            if (writeTimingHeader) {
                logger.info("[TIMING] Creating the timing file and writing the CSV header to the file: "
                        + _timingFile.getAbsolutePath());
                timing.writeHeader(timingWriter);
            }

            // STATS
            // Check if the stats file exists to know if the header should be written
            File _statsFile = new File(statsFile);
            Boolean writeStatsHeader = false;
            if (!_statsFile.exists()) {
                writeStatsHeader = true;
            }

            FileWriter statsWriter = new FileWriter(_statsFile, true);
            if (writeStatsHeader) {
                logger.info("[TIMING] Creating the stats file and writing the CSV header to the file: "
                        + _timingFile.getAbsolutePath());
                try {
                    statsWriter.write(MohawkT.getCSVHeader());
                } catch (IOException e) {
                    logger.severe("[STATS] Unable to write the Header to the file: " + _statsFile.getAbsolutePath());
                    statsWriter.close();
                    throw e;
                }
            }

            BooleanErrorListener errorListener = new BooleanErrorListener();
            MohawkT m;
            Integer claimedRoles = -1;
            String[] fileExts = fileExt.getFileExtensions();
            for (Integer j = 0; j < fileExts.length; j++) {
                String ext = fileExts[j];

                if (!testExtention(cmd, ext)) {
                    logger.info("[FILE EXT] Skipping file extension: " + ext);
                    continue;
                }
                logger.info("[FILE EXT] Processing file extension: " + ext);

                File[] files = new File[1];
                if (fileHelper.bulk) {
                    inputFolder = new File(fileHelper.specFile);
                    files = inputFolder.listFiles(FileExtensions.getFilter(ext));
                } else {
                    files[0] = new File(fileHelper.specFile);
                }
                for (Integer i = 1; i <= files.length; i++) {
                    File specFile = files[i - 1];
                    String timerStr = ext + " - specFile " + specFile.getAbsolutePath();

                    logger.info("[FILE] Processing file: " + specFile.getAbsolutePath());

                    /* Timing */timing.startTimer(timerStr);

                    m = null;
                    errorListener.errorFound = false;
                    InputStream is = new FileInputStream(specFile);
                    ANTLRInputStream input = new ANTLRInputStream(is);

                    // Can't use a switch statement because the file extensions must be dynamic
                    if (ext.equals(fileExt.Mohawk)) {
                        logger.info("[CONVERTING] Converting Mohawk file to Mohawk-T");
                        Mohawkv2Lexer lexer = new Mohawkv2Lexer(input);
                        CommonTokenStream tokens = new CommonTokenStream(lexer);
                        Mohawkv2Parser parser = new Mohawkv2Parser(tokens);

                        parser.removeErrorListeners();
                        parser.addErrorListener(errorListener);
                        parser.init();

                        if (errorListener.errorFound == false) {
                            m = parser.mohawkT;
                            claimedRoles = parser.claimedRoles;
                        }

                    } else if (ext.equals(fileExt.ASASPTime_SA)) {
                        logger.info("[CONVERTING] Converting ASASPTime SA file to Mohawk-T");
                        ASASPTimeSALexer lexer = new ASASPTimeSALexer(input);
                        CommonTokenStream tokens = new CommonTokenStream(lexer);
                        ASASPTimeSAParser parser = new ASASPTimeSAParser(tokens);

                        parser.removeErrorListeners();
                        parser.addErrorListener(errorListener);
                        parser.init();

                        if (errorListener.errorFound == false) {
                            m = parser.mohawkT;
                            claimedRoles = parser.claimedRoles;
                        }

                    } else if (ext.equals(fileExt.ASASPTime_NSA)) {
                        logger.info("[CONVERTING] Converting ASASPTime NSA file to Mohawk-T");
                        ASASPTimeNSALexer lexer = new ASASPTimeNSALexer(input);
                        CommonTokenStream tokens = new CommonTokenStream(lexer);
                        ASASPTimeNSAParser parser = new ASASPTimeNSAParser(tokens);

                        parser.removeErrorListeners();
                        parser.addErrorListener(errorListener);
                        parser.init();

                        if (errorListener.errorFound == false) {
                            m = parser.mohawkT;
                            claimedRoles = parser.claimedRoles;
                        }

                    } else {
                        // Can only enter this section if someone forgot to program something in
                        logger.warning("[SKIPPING] The file extension: '" + ext
                                + "' does not have a program capable of testing it!");

                        timing.removeTimer(timerStr);
                        continue;
                    }
                    logger.info("[CONVERTING] Done Converting");

                    if (m != null) {
                        if (displayStats) {
                            System.out.println("Roles: " + m.roleHelper);
                            System.out.println("Timeslots: " + m.timeIntervalHelper);
                            System.out.println("Max Timeslot: " + m.timeIntervalHelper.maxTimeSlot);
                            System.out.println(m.query.toString());
                            System.out.println(m.expectedResult.toString());
                            System.out.println(m.canAssign.toString());
                            System.out.println(m.canDisable.toString());
                            System.out.println(m.canEnable.toString());
                            System.out.println(m.canRevoke.toString());
                        }

                        if (writeStats) {
                            String sStr = m.getCSVString(specFile.getAbsolutePath(), claimedRoles);

                            logger.info("[FILE I/O] Writing Mohawk-T Stats to file: " + _statsFile.getAbsolutePath());
                            logger.info("Stats: " + sStr);

                            statsWriter.write(sStr);
                        }

                        if (writeMohawkTOutput) {
                            String mStr = m.getString("\n\n", true, true);

                            logger.info("[FILE I/O] Writing Mohawk-T to file: " + specFile.getAbsolutePath()
                                    + fileExt.Mohawk_T);
                            Files.write(Paths.get(specFile.getAbsolutePath() + fileExt.Mohawk_T), mStr.getBytes());
                        }
                    } else {
                        logger.warning("[PARSING ERROR] An error was found when trying to parse the input file");
                    }

                    /* Timing */timing.stopTimer(timerStr);
                    /* Timing */timing.writeOutLast(timingWriter);
                }
            }
            /* Timing */timing.stopTimer("totalTime");
            /* Timing */timing.writeOutLast(timingWriter);
            /* Timing */timingWriter.close();

            statsWriter.close();

            logger.info("[TIMING] " + timing.toString());
        } catch (Exception e) {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            logger.severe(errors.toString());
            logger.severe(e.getMessage());

            logger.info("[EOF] The Reverse Converter Instance is now done");
            return -1;
        }

        logger.info("[EOF] The Reverse Converter Instance is now done");
        return 0;
    }

    private Boolean testExtention(CommandLine cmd, String ext) {
        if (ext.equals(fileExt.Mohawk) && (cmd.hasOption(OptionString.FROM_MOHAWK.toString())
                || cmd.hasOption(OptionString.FROM_ALL.toString()))) {
            return true;
        } else if (ext.equals(fileExt.ASASPTime_NSA) && (cmd.hasOption(OptionString.FROM_ASASPTIME_NSA.toString())
                || cmd.hasOption(OptionString.FROM_ALL.toString()))) {
            return true;
        } else if (ext.equals(fileExt.ASASPTime_SA) && (cmd.hasOption(OptionString.FROM_ASASPTIME_SA.toString())
                || cmd.hasOption(OptionString.FROM_ALL.toString()))) { return true; }
        return false;
    }

    public void printHelp(CommandLine cmd, Options options) throws Exception {
        if (cmd.hasOption("maxw")) {
            try {
                Integer maxw = Integer.decode(cmd.getOptionValue("maxw"));
                printHelp(options, maxw);
            } catch (Exception e) {
                printHelp(options, 80);

                e.printStackTrace();
                throw new Exception("An error occured when trying to print out the help options!");
            }
        } else {
            printHelp(options, 80);
        }
    }

    public void printHelp(Options options, int maxw) {
        HelpFormatter f = new HelpFormatter();
        f.printHelp(maxw, "mohawk-converter",
                StringUtils.repeat("-", maxw) + "\nAuthors: " + AUTHORS + "\n" + StringUtils.repeat("-", 20), options,
                StringUtils.repeat("-", maxw), true);
    }

    @SuppressWarnings("static-access")
    public void setupOptions(Options options) {
        // Add Information Options
        options.addOption(OptionString.HELP.toString(), false, "Print this message");
        options.addOption(OptionString.AUTHORS.toString(), false, "Prints the authors");
        options.addOption(OptionString.VERSION.toString(), false, "Prints the version (" + VERSION + ") information");
        options.addOption(OptionString.CHECKNUSMV.toString(), false,
                "Checks that NuSMV is on the system and displays which version is installed");

        // Add Logging Level Options
        options.addOption(OptionBuilder.withArgName("quiet|debug|verbose")
                .withDescription("Be extra quiet only errors are shown; " + "Show debugging information; "
                        + "extra information is given for Verbose; " + "default is warning level")
                .hasArg().create(OptionString.LOGLEVEL.toString()));
        options.addOption(OptionBuilder.withArgName("logfile|'n'|'u'")
                .withDescription("The filepath where the log file should be created; "
                        + "No file will be created when equal to 'n'; "
                        + "A unique filename will be created when equal to 'u'; " + "default it creates a log called '"
                        + Logger_filepath + "'")
                .hasArg().create(OptionString.LOGFILE.toString()));
        options.addOption(OptionBuilder.withArgName("folder path")
                .withDescription("The path to the location that the logs should be placed, "
                        + "default it puts the logs in the folder '" + Logger_folderpath + "'")
                .hasArg().create(OptionString.LOGFOLDER.toString()));
        options.addOption(OptionString.NOHEADER.toString(), false,
                "Does not write the CSV file header to the output log");

        // custom Console Logging Options
        options.addOption(
                OptionBuilder.withArgName("num").withDescription("The maximum width of the console (default 120)")
                        .hasArg().create(OptionString.MAXW.toString()));
        options.addOption(OptionBuilder.withArgName("string")
                .withDescription("The new line string when wrapping a long line (default '\\n    ')").hasArg()
                .create(OptionString.LINESTR.toString()));

        // Add File IO Options
        options.addOption(OptionBuilder.withArgName("file|folder")
                .withDescription("Path to the Spec File, or Folder if the 'bulk' option is set").hasArg()
                .create(OptionString.SPECFILE.toString()));

        options.addOption(OptionBuilder.withArgName("extension")
                .withDescription(
                        "File extention used when searching for SPEC files when the 'bulk' option is used. Default:'"
                                + fileHelper.fileExt + "'")
                .hasArg().create(OptionString.SPECEXT.toString()));

        options.addOption(OptionString.SKIP_OUTPUT.toString(), false,
                "Skip writing the converted Mohawk+T file (use if you only want the stats of the converted file).");

        // Add Functional Options
        options.addOption(OptionString.BULK.toString(), false,
                "Use the folder that rbacspec points to and run against all *.spec");

        options.addOption(OptionString.FROM_ASASPTIME_NSA.toString(), false,
                "Convert input SPEC file from ASASPTime NSA format to Mohawk-T");
        options.addOption(OptionString.FROM_ASASPTIME_SA.toString(), false,
                "Convert input SPEC file from ASASPTime SA format to Mohawk-T");
        options.addOption(OptionString.FROM_TROLE.toString(), false,
                "Convert input SPEC file from TRole format to Mohawk-T");
        options.addOption(OptionString.FROM_TRULE.toString(), false,
                "Convert input SPEC file from TRule format to Mohawk-T");
        options.addOption(OptionString.FROM_MOHAWK.toString(), false,
                "Convert input SPEC file from Mohawk's ARBAC format to Mohawk-T");
        options.addOption(OptionString.FROM_ALL.toString(), false,
                "Convert input SPEC file from Mohawk, ASASPTime NSA, ASASPTime SA, TRole, TRule formats to Mohawk-T");
    }

    public Level getLoggerLevel() {
        return LoggerLevel;
    }

    public void setLoggerLevel(Level loggerLevel) {
        LoggerLevel = loggerLevel;
    }

    private void setupLoggerOptions(CommandLine cmd, Options options) throws SecurityException, IOException {
        // Logging Level
        logger.setUseParentHandlers(false);
        consoleHandler.setFormatter(new MohawkConsoleFormatter());
        setLoggerLevel(Level.FINE);// Default Level
        if (cmd.hasOption(OptionString.LOGLEVEL.toString())) {
            String loglevel = cmd.getOptionValue(OptionString.LOGLEVEL.toString());
            if (loglevel.equalsIgnoreCase("quiet")) {
                setLoggerLevel(Level.SEVERE);
            } else if (loglevel.equalsIgnoreCase("debug")) {
                setLoggerLevel(Level.FINEST);
            } else if (loglevel.equalsIgnoreCase("verbose")) {
                setLoggerLevel(Level.INFO);
            }
        }

        logger.setLevel(LoggerLevel);
        consoleHandler.setLevel(LoggerLevel);
        logger.addHandler(consoleHandler);

        // Add CSV File Headers
        if (cmd.hasOption(OptionString.NOHEADER.toString())) {
            WriteCSVFileHeader = false;
        }

        // Set Logger Folder
        if (cmd.hasOption(OptionString.LOGFOLDER.toString())) {
            Logger_folderpath = cmd.getOptionValue(OptionString.LOGFOLDER.toString());
        }

        // Set File Logger
        if (cmd.hasOption(OptionString.LOGFILE.toString())) {
            // Check if no log file was requested
            if (cmd.getOptionValue(OptionString.LOGFILE.toString()).equals("n")) {
                // Create no log file
                Logger_filepath = "";
            } else if (cmd.getOptionValue(OptionString.LOGFILE.toString()).equals("u")) {
                // Create a unique log file
                Logger_filepath = "mohawk-log.%u.%g.txt";
            } else {
                try {
                    // Create a log file with a specific name
                    File logfile = new File(
                            Logger_folderpath + File.separator + cmd.getOptionValue(OptionString.LOGFILE.toString()));

                    if (!logfile.exists()) {
                        logfile.createNewFile();
                    }
                    Logger_filepath = logfile.getAbsolutePath();

                    if (WriteCSVFileHeader) {
                        FileOutputStream writer = new FileOutputStream(logfile, true);// Always append!
                        writer.write(MohawkCSVFileFormatter.csvHeaders().getBytes());
                        writer.flush();
                        writer.close();
                    }

                } catch (IOException e) {
                    logger.severe(e.getMessage());
                    return;
                }
            }
        }
        // Add Logger File Handler
        if (!Logger_filepath.isEmpty()) {
            File logfile = new File(Logger_folderpath);
            logfile.mkdirs();

            if (!logfile.isDirectory()) {
                logger.severe("logfolder did not contain a folder that exists or that could be created!");
            }
            fileHandler = new FileHandler(Logger_folderpath + File.separator + Logger_filepath);
            fileHandler.setLevel(getLoggerLevel());
            fileHandler.setFormatter(new MohawkCSVFileFormatter());
            logger.addHandler(fileHandler);
        }
    }

    private Boolean setupReturnImmediatelyOptions(CommandLine cmd, Options options) throws Exception {
        if (cmd.hasOption(OptionString.HELP.toString()) == true || cmd.getOptions().length == 0) {
            printHelp(cmd, options);
            return true;
        }

        if (cmd.hasOption(OptionString.VERSION.toString())) {
            // keep it as simple as possible for the version
            System.out.println(VERSION);
            return true;
        }

        if (cmd.hasOption(OptionString.AUTHORS.toString())) {
            // keep it as simple as possible for the version
            System.out.println(AUTHORS);
            return true;
        }

        return false;
    }

    private void setupSpecOptions(CommandLine cmd, Options options) {
        // Grab the SPEC file
        if (cmd.hasOption(OptionString.SPECFILE.toString())) {
            logger.fine("[OPTION] Using a specific SPEC File: " + cmd.getOptionValue(OptionString.SPECFILE.toString()));
            fileHelper.specFile = cmd.getOptionValue(OptionString.SPECFILE.toString());
        } else {
            logger.fine("[OPTION] No Spec File included");
        }

        if (cmd.hasOption(OptionString.SPECEXT.toString())) {
            logger.fine("[OPTION] Using a specific SPEC File Extension: "
                    + cmd.getOptionValue(OptionString.SPECEXT.toString()));
            fileHelper.fileExt = cmd.getOptionValue(OptionString.SPECEXT.toString());
        } else {
            logger.fine("[OPTION] Using the default SPEC File Extension: " + fileHelper.fileExt);
        }

        // Load more than one file from the SPEC File?
        if (cmd.hasOption(OptionString.BULK.toString())) {
            logger.fine("[OPTION] Bulk SPEC File inclusion: Enabled");
            fileHelper.bulk = true;
        } else {
            logger.fine("[OPTION] Bulk SPEC File inclusion: Disabled");
        }

        // Skip writing the Mohawk+T file to disk?
        if (cmd.hasOption(OptionString.SKIP_OUTPUT.toString())) {
            logger.fine("[OPTION] Skipping writing the Mohawk+T file to disk: Enabled");
            writeMohawkTOutput = false;
        } else {
            logger.fine("[OPTION] Skipping writing the Mohawk+T file to disk: Disabled");
            writeMohawkTOutput = true;
        }
    }

    private void setupUserPreferenceOptions(CommandLine cmd, Options options) {
        // Set the Console's Max Width
        if (cmd.hasOption(OptionString.MAXW.toString())) {
            logger.fine("[OPTION] Setting the console's maximum width");
            String maxw = "";
            try {
                maxw = cmd.getOptionValue(OptionString.MAXW.toString());
                ((MohawkConsoleFormatter) consoleHandler.getFormatter()).maxWidth = Integer.decode(maxw);
            } catch (NumberFormatException e) {
                logger.severe("[ERROR] Could not decode 'maxw': " + maxw + ";\n" + e.getMessage());
            }
        } else {
            logger.fine("[OPTION] Default Console Maximum Width Used");
        }

        // Set the Console's Wrap String
        if (cmd.hasOption(OptionString.LINESTR.toString())) {
            logger.fine("[OPTION] Setting the console's new line string");
            ((MohawkConsoleFormatter) consoleHandler.getFormatter()).newLineStr = cmd
                    .getOptionValue(OptionString.LINESTR.toString());
        } else {
            logger.fine("[OPTION] Default Line String Used");
        }

    }
}
