package mohawk.converter.reverse.testing;

import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import org.antlr.v4.runtime.*;

public class BooleanErrorListener extends BaseErrorListener {
    public final Logger logger = Logger.getLogger("mohawk");
    public Boolean errorFound = false;

    @Override
    public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine,
            String msg, RecognitionException e) {
        List<String> stack = ((Parser) recognizer).getRuleInvocationStack();
        Collections.reverse(stack);
        System.err.println("rule stack: " + stack);
        System.err.println("line " + line + ":" + charPositionInLine + " at " + offendingSymbol + ": " + msg);

        errorFound = true;
    }

    @Override
    public String toString() {
        return errorFound.toString();
    }
}
