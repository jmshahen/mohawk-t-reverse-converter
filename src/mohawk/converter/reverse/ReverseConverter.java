package mohawk.converter.reverse;

import java.util.logging.Logger;

public class ReverseConverter {
    public static final Logger logger = Logger.getLogger("mohawk");

    public static void main(String[] args) {
        ReverseConverterInstance inst = new ReverseConverterInstance();

        inst.run(args);
    }

}
